-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 23 juin 2019 à 17:48
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `plant_api`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `color` varchar(15) COLLATE utf8_bin NOT NULL DEFAULT 'light',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `color`) VALUES
(1, 'Plante d\'intérieur', 'primary'),
(2, 'Plante d\'extérieur', 'light');

-- --------------------------------------------------------

--
-- Structure de la table `history`
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_id` int(11) NOT NULL,
  `humidity` int(11) NOT NULL,
  `temperature` int(11) NOT NULL,
  `luminosity` int(11) NOT NULL,
  `checkdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_history_plant` (`plant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `history`
--

INSERT INTO `history` (`id`, `plant_id`, `humidity`, `temperature`, `luminosity`, `checkdate`) VALUES
(1, 3, 5, 19, 68775, '2019-06-23 19:35:12'),
(2, 3, 3, 25, 82870, '2019-06-23 20:35:12'),
(3, 3, 2, 17, 35844, '2019-06-23 21:35:12'),
(4, 3, 5, 16, 27266, '2019-06-23 22:35:12'),
(5, 3, 9, 17, 48862, '2019-06-23 23:35:12'),
(6, 3, 6, 20, 17001, '2019-06-24 00:35:12'),
(7, 3, 6, 18, 17634, '2019-06-24 01:35:12'),
(8, 3, 0, 25, 45079, '2019-06-24 02:35:12'),
(9, 3, -1, 15, 66316, '2019-06-23 19:36:29'),
(10, 3, 6, 19, 38579, '2019-06-23 19:36:37'),
(11, 3, 4, 22, 61426, '2019-06-23 19:36:43'),
(12, 3, 4, 24, 72258, '2019-06-23 19:36:49');

-- --------------------------------------------------------

--
-- Structure de la table `pictures`
--

DROP TABLE IF EXISTS `pictures`;
CREATE TABLE IF NOT EXISTS `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_id` int(11) NOT NULL,
  `path` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pictures_plant` (`plant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `pictures`
--

INSERT INTO `pictures` (`id`, `plant_id`, `path`) VALUES
(1, 1, 'sansevieria.jpg'),
(2, 2, 'lierrecannarie.jpg'),
(3, 3, 'hortensia.jpg'),
(4, 4, 'bambou.jpg'),
(5, 5, 'yucca.jpg'),
(6, 6, 'chlorophytum.jpg'),
(8, 12, 'custom_aloe-vera-pour-le-corps.jpg'),
(9, 13, 'custom_Canariensis_group_1400x.jpg'),
(10, 14, 'custom_pl2000028828.jpg'),
(11, 15, 'custom_Bonsai.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `plants`
--

DROP TABLE IF EXISTS `plants`;
CREATE TABLE IF NOT EXISTS `plants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `isCurrent` tinyint(1) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8_bin NOT NULL,
  `humidity` int(11) NOT NULL,
  `luminosity` int(11) NOT NULL,
  `temperature` int(11) NOT NULL,
  `flowering` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `plants`
--

INSERT INTO `plants` (`id`, `name`, `isCurrent`, `description`, `humidity`, `luminosity`, `temperature`, `flowering`) VALUES
(1, 'Sansevieria', 0, 'Sansevieria est un genre de 70 espèces de plantes succulentes, généralement herbacées. Il appartient à la famille des Liliaceae (ou à celle des Dracaenaceae) selon la classification classique', 1, 100000, 20, '2019-08-21'),
(2, 'Lierre des Cannaries', 0, 'Le lierre grimpant est une espèce de lianes arbustive à feuilles persistantes. Le lierre des canaries Originaire d’Afrique du Nord, est peu rustique (jusqu’à – 12 °C). Il se reconnaît aux tiges et aux revers des jeunes feuilles couverts de poils rouges.', 20, 25000, 3, '2019-07-09'),
(3, 'Hortensia', 1, 'L’hortensia est une plante rustique et vavica qui profite d’une culture et d’un entretien facile. Il enjolivera un massif, constituera une élégante haie fleurie ou apportera une touche de couleur à votre intérieur.\r\n', 4, 50000, 20, '2019-07-17'),
(4, 'Bambou', 0, 'Les bambous sont des plantes monocotylédones appartenant à la famille des Poaceae (graminées), sous-famille des Bambusoideae. Ils se distinguent des autres graminées par leur port arborescent et leurs tiges ligneuses souvent de grande longueur.', 0, 100000, 30, '2019-06-13'),
(5, 'Yucca', 0, 'Le yucca est grand classique pour l’intérieur que l’on apprécie par sa facilité d’entretien et sa rusticité. \r\nBien que de croissance lente, le yucca peut atteindre près de 1,5m de hauteur. Il sera idéal dans une pièce lumineuse voire ensoleillée, la véranda ou le séjour par exemple. ', 2, 75000, 25, '2019-06-19'),
(6, 'Chlorophytum', 0, 'Belle vivace au port élégant et au feuillage ornemental, le Chlorophytum bonnie fera assurément la beauté de vos terrasses, mais aussi de tous vos intérieurs. D’une culture très facile, cette plante est peu exigeante et répondra à toutes vos attentes en matière d’esthétisme et de raffinement', 5, 50000, 20, '2019-06-19'),
(12, 'Aloe Vera', 0, 'L’aloe vera forme une touffe graphique composée de feuilles succulentes gris-vert légèrement tachetées. Il peut lui arriver de fleurir en été quand il bénéficie de suffisamment de lumière. Il apprécie les situations chaudes en été et accepte en hiver des températures plus fraîches (autour de 10°C). ', 20, 100000, 25, '2019-06-23'),
(13, 'Phoenix canariensis', 0, 'Le palmier des Canaries, emblématique de la côte d\'Azur, orne promenades de bord de mer, parcs et places du Sud. Victime de parasites, il disparaît peu à peu du paysage méditerranéen, mais constitue toujours un sujet ornemental exceptionnel dans un jardin.', 3, 120000, 28, '2019-06-23'),
(14, 'Kentia Howea', 0, 'Palmier couramment cultivé en intérieur, le Kentia (Howea) apporte une note d’élégance et de verticalité à votre décor. En appartement lumineux, dans un bureau mais aussi dans un jardin d\'hiver ou une véranda, il saura vous charmer à coup sûr !', 5, 100000, 20, '2019-06-23'),
(15, 'Bonzaï', 0, 'Le bonsaï ou bonzaï, culture miniaturisante de végétaux, est un art traditionnel japonais dérivé de l\'art originaire chinois du penjing. Cette pratique se retrouve dans les cultures des pays voisin de cette région du monde comme le Vietnam ou la Corée.', 5, 20, 19, '2019-06-23');

-- --------------------------------------------------------

--
-- Structure de la table `plants_category`
--

DROP TABLE IF EXISTS `plants_category`;
CREATE TABLE IF NOT EXISTS `plants_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cat_plants` (`plant_id`),
  KEY `fk_cat_cat` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `plants_category`
--

INSERT INTO `plants_category` (`id`, `plant_id`, `category_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2),
(4, 4, 2),
(5, 5, 1),
(6, 6, 1),
(13, 12, 1),
(14, 12, 2),
(15, 13, 2),
(16, 14, 1),
(17, 15, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `fk_history_plant` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`);

--
-- Contraintes pour la table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `fk_pictures_plant` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`);

--
-- Contraintes pour la table `plants_category`
--
ALTER TABLE `plants_category`
  ADD CONSTRAINT `fk_cat_cat` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `fk_cat_plants` FOREIGN KEY (`plant_id`) REFERENCES `plants` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
