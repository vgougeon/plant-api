#include <Bridge.h>
#include <HttpClient.h>

class Sensor {
  private:
  	byte m_port;
  public:
  	Sensor(byte port);
  	float getValue();
};
Sensor::Sensor(byte port){
  m_port = port;
}
float Sensor::getValue(){
  return analogRead(m_port);
}

Sensor luminosity(A0);
Sensor temperature(A1);
Sensor humidity(A2);

void setup() {
  Serial.begin(9600);
}

void loop() {
  
  HttpClient client;
  client.get("http://localhost/plant-api/site/arduino");
  
  Serial.println(luminosity.getValue());
  Serial.println(temperature.getValue());
  Serial.println(humidity.getValue());
  //Verifier toutes les heures
  delay(3600000);
}