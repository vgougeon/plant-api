<?php require("../model/models.php");
if(isset($_GET['action']) and $_GET['action'] == "" or $_GET['action'] == "home"){
    $title = "World of plants";
    $css = ["home.css"];
    
    $currentPlant = getCurrentPlant();
    $history = getHistory($currentPlant["id"]);
    include("../view/home.php");
}
else if(isset($_GET['action']) and $_GET['action'] == "plants"){
    $title = "Changer de plante";
    $css = ["home.css"];

    $currentPlant = getCurrentPlant();
    $plants = getPlants();

    include("../view/plants.php");
}
else if(isset($_GET['action']) and $_GET['action'] == "add"){
    $title = "Nouvelle plante";
    $css = ["home.css"];

    $categories = getCategories();

    include("../view/add.php");
}
else if(isset($_GET['action']) and $_GET['action'] == "history"){

    $currentPlant = getCurrentPlant();
    $history = getHistory($currentPlant["id"]);
    $title = "Historique";
    $css = [];

    include("../view/history.php");
}
else if(isset($_GET['action']) and $_GET['action'] == "choosePlant"){
    changePlantTo($_POST['id']);
}
else if(isset($_GET['action']) and $_GET['action'] == "newPlant"){
    newPlant($_POST, $_FILES);
}
else if(isset($_GET['action']) and $_GET['action'] == "notification"){
    include("../view/modules/notification.php");
}
else {
    echo "404";
}
