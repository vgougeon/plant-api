
$('[data-id][data-action]').on("click", function(){
    id = $(this).data("id")
    $.ajax({
        method: "POST",
        url: $(this).data("action"),
        data: { id: id }
    })
    .done(function( data ) {
        console.log(data)
        notification("fas fa-globe", data)
        $(".main_object[data-id=" + id + "]").hide();
    });
})

$('[data-action=choosePlant]').on("click", function(){
    let id = $(this).data("id")
    $("aside .card img").attr("src", $(".card[data-id=" + id + "] img").attr("src"))
    $("aside .card h5").text($(".card[data-id=" + id + "] h5").text())
})

function notification(icon, title) {
	if ($('.fluid_notification').length){
        $('.fluid_notification').remove();
    }
	$.post("notification", { icon: icon, title: title },
	function(data) {
		$('body').append(data);
	}
	);
}
$(".searchPlant").on("keyup", function(){
    if($(".searchPlant").val().length > 0){
        $(".card[data-id][data-name]").not("[data-name^='" + $(".searchPlant").val() + "']").parent().hide()
        $(".card[data-id][data-name^='" + $(".searchPlant").val() + "']").parent().show()
    }
    if($(".searchPlant").val().length == 0) {
        $(".card[data-id][data-name]").parent().show()
    }
})
