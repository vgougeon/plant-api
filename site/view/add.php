<?php
include("modules/head.php");
include("modules/header.php"); ?>
<section class="content">
<form enctype="multipart/form-data" action="newPlant" method="POST">
    <div class="form-group">
        <label for="name">Nom de la plante</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Nom">
    </div>
    <div class="form-group">
        <label for="categories">Catégories</label>
        <select multiple class="form-control" id="categories" name="categories[]">
            <?php foreach($categories as $item){ ?>
                <option value="<?php echo $item["id"]; ?>"><?php echo $item["name"]; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-lg-4">
                <label for="humidity">Humidité optimale</label>
                <input type="text" class="form-control" id="humidity" name="humidity" placeholder="Humidité">
            </div>
            <div class="col-lg-4">
                <label for="luminosity">Luminosité optimale</label>
                <input type="text" class="form-control" id="luminosity" name="luminosity" placeholder="Luminosité">
            </div>
            <div class="col-lg-4">
                <label for="temperature">Température optimale</label>
                <input type="text" class="form-control" id="temperature" name="temperature" placeholder="Température">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" id="description" name="description" rows="3"></textarea>
    </div>
    <div class="custom-file">
        <input type="file" class="custom-file-input" id="customFile" name="image">
        <label class="custom-file-label" for="customFile">Choisir une image</label>
    </div>
    <input type="submit" class="btn btn-primary mt-3" value="Créer !"/>
</form>
</section>
<?php include("modules/footer.php"); ?>
