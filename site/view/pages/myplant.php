<section class="content">
    <div class="row">
        <div class="col-md-6 card-container mb-3">
            <div class="card w-100">
                <img src="assets/imgs/plants/<?php
                    if(!empty($currentPlant["pictures"][0])){
                        echo $currentPlant["pictures"][0]["path"];
                    }
                    else {
                        echo "default.jpg";
                    }
                ?>" class="card-img-top" alt="Ma plante">
                <div class="card-body">
                    <h5 class="card-title mb-1"><?php echo $currentPlant["name"]; ?></h5>
                    <?php foreach($currentPlant["categories"] as $category){?>
                    <div class="badge badge-<?php echo $category["color"]; ?> mt-0"><?php echo $category["name"]; ?></div>
                    <?php } ?>
                    <p class="card-text"><?php echo $currentPlant["description"]; ?></p>
                    <a href="history" class="btn btn-outline-dark">Voir l'historique</a>
                    <a href="plants" class="btn btn-outline-primary">Changer</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 card-container mb-3">
            <div class="card w-100">
                <div class="card-body">
                    <h5 class="card-title mb-1">Entretien & conseils</h5>
                    <?php foreach($currentPlant["categories"] as $category){?>
                    <div class="badge badge-<?php echo $category["color"]; ?> mt-0"><?php echo $category["name"]; ?></div>
                    <?php } ?>
                    <p class="card-text mt-2">Bienvenue sur votre espace <strong>World of Plants</strong> ! Ici vous pouvez trouver des conseils sur l'entretien et la plantation
                    de vos plantes, mais aussi découvrir de nouvelles plantes interessantes dans l'onglet <strong>"Changer de plante"</strong></p>
                    <?php
                    $diff = $currentPlant["humidity"] - end($history)["humidity"];
                    if(abs($diff) > 3){ ?>
                        <div class="alert alert-dark" role="alert">
                            Attention ! Le niveau d'humidité est éloigné de celui préconisé pour votre plante. (Différence de <?php echo $diff; ?> g/m3).
                        </div>
                    <?php }
                    $diff = $currentPlant["temperature"] - end($history)["temperature"];
                    if(abs($diff) > 3){ ?>
                        <div class="alert alert-dark" role="alert">
                            Attention ! La température est éloignée de celle préconisée pour votre plante. (Différence de <?php echo $diff; ?>°C).
                        </div>
                    <?php }
                    $diff = $currentPlant["luminosity"] - end($history)["luminosity"];
                    if(abs($diff) > 35000){ ?>
                        <div class="alert alert-dark" role="alert">
                            Attention ! La luminosité est éloignée de celle préconisée pour votre plante. (Différence de <?php echo $diff; ?> lux).
                        </div>
                    <?php }
                    ?>
                    <h5 class="card-title mb-3"><?php echo $currentPlant["name"]; ?></h5>
                    <p class="card-text mt-2 text-primary"><strong>Température optimale</strong> : <?php echo $currentPlant["temperature"]; ?>°C</p>
                    <p class="card-text mt-2 text-primary"><strong>Luminosité optimale</strong> : <?php echo $currentPlant["luminosity"]; ?> lux</p>
                    <p class="card-text mt-2 text-primary"><strong>Humidité optimale</strong> : <?php echo $currentPlant["humidity"]; ?> g/m3</p>

            </div>
        </div>
    </div>
</section>
