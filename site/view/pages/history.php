<section class="content">
    <div class="charts row">
        <div class="col-xl-6 col-lg-12">
            <canvas id="luminosity" style="height: 30%;"></canvas>
        </div>
        <div class="col-xl-6 col-lg-12">
            <canvas id="humidity" style="height: 30%;"></canvas>
        </div>
        <div class="col-xl-6 col-lg-12">
            <canvas id="temperature" style="height: 30%;"></canvas>
        </div>
    </div>
</section>
