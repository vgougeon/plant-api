<section class="content">
    <div class="d-flex flex-wrap justify-content-between">
        <a href="add" class="btn btn-outline-primary mb-3">Ajouter une plante</a>
        <form class="form-inline mx-2">
          <input class="form-control mr-sm-2 searchPlant" type="search" placeholder="Rechercher">
          <button class="btn btn-outline-success my-sm-0" type="submit">Go</button>
        </form>
    </div>
    <div class="row">
    <?php foreach($plants as $plant){ ?>
        <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 card-container mb-3">
            <div class="card w-100" data-name="<?php echo $plant["name"]; ?>" data-id="<?php echo $plant["id"]; ?>">
                <img src="assets/imgs/plants/<?php
                    if(!empty($plant["pictures"][0])){
                        echo $plant["pictures"][0]["path"];
                    }
                    else {
                        echo "default.jpg";
                    }
                ?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title mb-1"><?php echo $plant["name"]; ?></h5>
                    <?php foreach($plant["categories"] as $category){?>
                    <div class="badge badge-<?php echo $category["color"]; ?> mt-0"><?php echo $category["name"]; ?></div>
                    <?php } ?>
                    <p class="card-text"><?php echo $plant["description"]; ?></p>
                    <a href="#" data-action="choosePlant" data-id="<?php echo $plant["id"]; ?>" class="btn btn-outline-dark">Choisir</a>
                    <a href="#" class="btn btn-outline-primary">Planifier</a>
                </div>

            </div>
        </div>
    <?php } ?>
    </div>
</section>
