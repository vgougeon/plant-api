<script>
let luminosity = {
    type: 'line',
    data: {
        labels:[
        <?php foreach($history as $item){ echo '"' . date('d/m H:i', strtotime($item["checkdate"])) . '",'; } ?>
        ],
        datasets: [{
            label: 'Luminosité',
            backgroundColor: "#2f9969",
            borderColor: "#2f9969",
            fill: false,
            data: [
                <?php foreach($history as $item){ echo '{y: '.$item["luminosity"].'},'; } ?>
            ]
        },
        {
            label: 'Luminosité optimale',
            backgroundColor: "#4c9ad2",
            borderColor: "#4c9ad2",
            fill: false,
            data: [
                <?php foreach($history as $item){ echo '{y: '.$currentPlant["luminosity"].'},'; } ?>
            ]
        }
        ]
    },
    options: {
		responsive: false,
		title: {
			display: false,
			text: 'Luminosité'
		},
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Date'
				},
				ticks: {
					major: {
						fontStyle: 'bold',
						fontColor: '#FF0000'
					}
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Luminosité'
				}
			}]
		}
	}
}
let humidity = {
    type: 'line',
    data: {
        labels:[
        <?php foreach($history as $item){ echo '"' . date('d/m H:i', strtotime($item["checkdate"])) . '",'; } ?>
        ],
        datasets: [{
            label: 'Humidité',
            backgroundColor: "#2f9969",
            borderColor: "#2f9969",
            fill: false,
            data: [
                <?php foreach($history as $item){ echo '{y: '.$item["humidity"].'},'; } ?>
            ]
        },
        {
            label: 'Humidité optimale',
            backgroundColor: "#4c9ad2",
            borderColor: "#4c9ad2",
            fill: false,
            data: [
                <?php foreach($history as $item){ echo '{y: '.$currentPlant["humidity"].'},'; } ?>
            ]
        }
        ]
    },
    options: {
		responsive: false,
		title: {
			display: false,
			text: 'Humidité'
		},
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Date'
				},
				ticks: {
					major: {
						fontStyle: 'bold',
						fontColor: '#FF0000'
					}
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Humidité optimale'
				},
                ticks: {
                    suggestedMin: <?php echo $currentPlant["humidity"] - 6; ?>,
                    suggestedMax: <?php echo $currentPlant["humidity"] + 6; ?>
                }
			}]
		}
	}
}
let temperature = {
    type: 'line',
    data: {
        labels:[
        <?php foreach($history as $item){ echo '"' . date('d/m H:i', strtotime($item["checkdate"])) . '",'; } ?>
        ],
        datasets: [{
            label: 'Température',
            backgroundColor: "#2f9969",
            borderColor: "#2f9969",
            fill: false,
            data: [
                <?php foreach($history as $item){ echo '{y: '.$item["temperature"].'},'; } ?>
            ]
        },
        {
            label: 'Température optimale',
            backgroundColor: "#4c9ad2",
            borderColor: "#4c9ad2",
            fill: false,
            data: [
                <?php foreach($history as $item){ echo '{y: '.$currentPlant["temperature"].'},'; } ?>
            ]
        }
        ]
    },
    options: {
		responsive: false,
		title: {
			display: false,
			text: 'Température'
		},
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Date'
				},
				ticks: {
					major: {
						fontStyle: 'bold',
						fontColor: '#FF0000'
					}
				}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Température'
				},
                ticks: {
                    suggestedMin: <?php echo $currentPlant["temperature"] - 6; ?>,
                    suggestedMax: <?php echo $currentPlant["temperature"] + 6; ?>
                }
			}]
		}
	}
}
new Chart($("#luminosity"), luminosity)
new Chart($("#humidity"), humidity)
new Chart($("#temperature"), temperature)
</script>
