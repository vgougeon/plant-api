<header>
    <div class="item">
        <div id="logo">
            <img src="assets/imgs/flower.png"/>
        </div>
        <h1>World of Plants</h1>
    </div>
</header>
<aside>
    <a href="home">
        <div class="button">
            <i class="fas fa-home"></i>
            <span>Accueil</span>
        </div>
    </a>
    <a href="history">
        <div class="button">
            <i class="fas fa-clock"></i>
            <span>Historique</span>
        </div>
    </a>
    <a href="plants">
        <div class="button">
            <i class="fas fa-leaf"></i>
            <span>Changer de plante</span>
        </div>
    </a>
    <div class="card m-2">
    <?php if(isset($currentPlant["name"])){ ?>
        <img src="assets/imgs/plants/<?php echo $currentPlant["pictures"][0]["path"]; ?>" class="card-img-top" alt="...">
        <div class="card-body p-3">
            <h5 class="card-title m-0"><?php echo $currentPlant["name"]; ?></h5>
            <p class="card-text m-0"><small class="text-muted">Plante actuelle</small></p>
        </div>
    <?php } ?>
        <?php if(!isset($currentPlant["name"])){ ?>
        <div class="card-footer d-flex justify-content-end">
            <button type="button" class="btn btn-primary btn-sm">Ajouter une plante</button>
        </div>
        <?php } ?>
    </div>
</aside>
