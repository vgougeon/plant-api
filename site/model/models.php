<?php require("database.php");

function getCurrentPlant(){
    global $db;
    $plants = array();
    $query = $db->query("SELECT * FROM plants WHERE isCurrent = 1");
    $res = $query->fetch();
    $res["categories"] = Array();
    $cat = $db->query("SELECT c.name, c.color FROM plants p INNER JOIN plants_category pc ON pc.plant_id = p.id INNER JOIN categories c ON c.id = pc.category_id WHERE p.id = " . $res['id']);
    while($cat_res = $cat->fetch()){
        array_push($res["categories"], $cat_res);
    }
    $res["pictures"] = Array();
    $pics = $db->query("SELECT i.path FROM plants p INNER JOIN pictures i ON i.plant_id = p.id WHERE p.id = " . $res['id']);
    while($pic_res = $pics->fetch()){
        array_push($res["pictures"], $pic_res);
    }
    return $res;
}
function getPlants(){
    global $db;
    $plants = array();
    $query = $db->query("SELECT * FROM plants");
    while($res = $query->fetch()){
        $res["categories"] = Array();
        $cat = $db->query("SELECT c.name, c.color FROM plants p INNER JOIN plants_category pc ON pc.plant_id = p.id INNER JOIN categories c ON c.id = pc.category_id WHERE p.id = " . $res['id']);
        while($cat_res = $cat->fetch()){
            array_push($res["categories"], $cat_res);
        }
        $res["pictures"] = Array();
        $pics = $db->query("SELECT i.path FROM plants p INNER JOIN pictures i ON i.plant_id = p.id WHERE p.id = " . $res['id']);
        while($pic_res = $pics->fetch()){
            array_push($res["pictures"], $pic_res);
        }
        array_push($plants, $res);
    }
    return $plants;
}
function getHistory($id){
    global $db;
    $history = array();
    $query = $db->query("SELECT * FROM (SELECT * FROM history WHERE plant_id = " . $id . " ORDER BY checkdate DESC LIMIT 10)last ORDER BY id ASC");
    while($item = $query->fetch()){
        array_push($history, $item);
    }
    return $history;
}
function changePlantTo($id){
    global $db;
    $req = $db->query("UPDATE plants SET isCurrent=0 WHERE isCurrent = 1");
    $req->execute();

    $req = $db->prepare("UPDATE plants SET isCurrent = 1 WHERE id = ?");
    $req->execute(
        [$id]
    );
    echo "Vous avez choisi une nouvelle plante !";
}
function getCategories(){
    global $db;
    $categories = array();
    $query = $db->query("SELECT * FROM categories");
    while($res = $query->fetch()){
        array_push($categories, $res);
    }
    return $categories;
}

function newPlant($post, $file){
    global $db;
    $req = $db->prepare("INSERT INTO plants (name, description, humidity, luminosity, temperature, flowering) VALUES (:name, :description, :humidity, :luminosity, :temperature, :flowering)");
    $req->execute(array(
        "name" => $post["name"],
        "description" => $post["description"],
        "humidity" => $post["humidity"],
        "luminosity" => $post["luminosity"],
        "temperature" => $post["temperature"],
        "flowering" => date("Y-m-d")
    ));
    $id = $db->lastInsertId();
    foreach($post["categories"] as $item){
        $req = $db->prepare("INSERT INTO plants_category (plant_id, category_id) VALUES (:plant_id, :category_id)");
        $req->execute(array(
            "plant_id" => $id,
            "category_id" => $item
        ));
    }
    move_uploaded_file($file['image']['tmp_name'], "../public/assets/imgs/plants/custom_" . $file['image']['name']);

    $req = $db->prepare("INSERT INTO pictures (plant_id, path) VALUES (:plant_id, :path)");
    $req->execute(array(
        "plant_id" => $id,
        "path" => "custom_" . $file['image']['name']
    ));
    header("Location: home");
}
