import pymysql
from random import randint
class CurrentPlant:
    def __init__(cls):
        cls.db = pymysql.connect(host='localhost',
                                     user='root',
                                     password='',
                                     db='plant_api',
                                     autocommit=True)
        cls.cur = cls.db.cursor(pymysql.cursors.DictCursor)
        cls.cur.execute("SELECT * FROM plants WHERE isCurrent = 1")
        cls.row = cls.cur.fetchone()

        cls.id = cls.row["id"]
        cls.humidity = cls.row["humidity"]
        cls.luminosity = cls.row["luminosity"]
        cls.temperature = cls.row["temperature"]
    def addEntry(cls, checkdate):
        cls.cur.execute("INSERT INTO history(plant_id, humidity, temperature, luminosity, checkdate) VALUES (" + str(cls.id) + ", " + str(cls.humidity + randint(-5,5)) + ", " + str(cls.temperature + randint(-5,5)) + ", " + str(cls.luminosity + randint(-40000,40000)) + ", '" + checkdate + "')")
    def resetHistory(cls):
        cls.cur.execute("TRUNCATE TABLE history")
