import pymysql
from random import randint
class History:
    def __init__(cls):
        cls.db = pymysql.connect(host='localhost',
                                     user='root',
                                     password='',
                                     db='plant_api',
                                     autocommit=True)
        cls.cur = cls.db.cursor(pymysql.cursors.DictCursor)
        cls.cur.execute("SELECT * FROM history WHERE converted = 0")
        cls.row = cls.cur.fetchall()
    def convert(cls):
        for item in cls.row:
            luminosity = item["luminosity"]*200
            humidity = item["humidity"]/100
            temperature = (item["temperature"] - 106) / 1.864
            sql = "UPDATE history SET luminosity = " + str(luminosity) + ", humidity = " + str(humidity) + ", temperature = " + str(temperature) + ", converted = 1 WHERE id = " + str(item["id"]) + ""
            print(sql)
            cls.cur.execute(sql)
