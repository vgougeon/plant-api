from random import randint
from datetime import datetime, timedelta
from src.currentPlant import CurrentPlant

plant = CurrentPlant()

entry_number = int(input("Combien voulez vous ajouter d'entrées pour l'historique de la plante actuelle ?\n"))
def daterange(start_date, end_date):
    delta = timedelta(hours=1)
    while start_date < end_date:
        yield start_date
        start_date += delta

for single_date in daterange(datetime.now(), datetime.now() + timedelta(hours = entry_number)):
    checkdate = single_date.strftime("%Y-%m-%d %H:%M:%S")
    plant.addEntry(checkdate)
