#Les capteurs donnent des valeurs qui ne correspondent pas aux unités degrés, ...
#Pour pallier ce problème, ce script sera executé toutes les heures grâce à CRON et
#convertira les valeurs avec un facteur qui permettra de retrouver des valeurs réalistes.
from src.history import History
hist = History()
hist.convert()
